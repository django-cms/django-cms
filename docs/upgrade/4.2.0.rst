.. _upgrade-to-enter-version-here-template:

###################
4.2.0 release notes
###################

*March 31, 2025*

Welcome to django CMS 4.2.0!

These release notes cover the new features, as well as some backwards
incompatible changes you’ll want to be aware of when upgrading from
django CMS 4.1 or earlier. We’ve begun the deprecation process for some
features.

See the How to upgrade to 4.2.0 to a newer version guide if you’re
updating an existing project.

Django and Python compatibility
===============================

django CMS supports **Django 4.2, 5.0, and 5.1**. We highly recommend and only
support the latest release of each series.

It supports **Python 3.10, 3.11, and 3.12**. As for Django we highly recommend and only
support the latest release of each series.

***********************
How to upgrade to 4.2.0
***********************

*******************
What's new in 4.2.0
*******************

Feature 1
=========

Empty sections are to be removed before release.


Security: Improved content security policy support
==================================================

Earlier versions of django CMS added inline JavaScript to the page in edit mode to 
communicate with the frontend editor. This effectively barred projects from enforcing
meaningful content security policies. In django CMS 4.2.0, we have removed all inline 
JavaScript from the edit mode (or other places in django CMS), replacing it with 
text/json objects to communicate with the frontend editor. This allows projects to 
enforce strict Content Security Policies (CSP) without any issues. 

For a fully working project, it is also important that other packages used, especially 
plugins, do not rely on inline JavaScript. This change enhances the security 
posture of your django CMS projects by enabling the use of CSP headers to mitigate 
cross-site scripting (XSS) and other code injection attacks.

Development: Exception handling
===============================

Since django CMS 4, exceptions that happen during plugin rendering have been
caught and displayed a message at the plugin's position. After feedback from
the community, django CMS 4.2 refactored exception handling.

* Exceptions are now caught on placeholder level.

* In edit mode, a message about the exception is shown for the placeholder. If
  ``settings.DEBUG == True`` this message includes the full Django trace.

* Editors still can edit plugins causing the exception. It can be edited by
  double-clicking the error message or through the structure board.

* In preview mode and on site, the placeholder containing the plugin will
  render empty.

* If ``settings.CMS_CATCH_PLUGIN_500_EXCEPTION`` is set to ``False``, trying
  to view content that raises an exception will trigger a server error
  (http 500). Preview and edit modes will still work.


Minor features
==============

* Deleting pages or deleting translations now gives a much clear delete
  confirmation message. It does not list all objects deleted but summarizes
  how many pages, translations (counted by ``PageUrl`` objects) and plugins
  are about to be deleted.

* ``CMS_PLACEHOLDER_CONF`` now allows to add configuration by template name for
  placeholders that not necessarily are part of a page, but could be part of
  any model (e.g., an alias). Instead for looking at pages, the placeholder tries
  to access a ``get_template()`` method on its source model instance to identify
  the template name its rendered on.


Bug Fixes
=========

**************************************
Backward incompatible changes in 4.2.0
**************************************

Merging of Page.node into Page
==============================

To improve performance and simplify database accesses, the ``TreeNode`` model
has been merged into the ``Page`` model. This change is backward incompatible
and will require a database migration.

Compatibility shims have been added to the ``Page`` model to ensure that custom
code that accesses the ``Page.node`` attribute will continue to work. However,
this compatibility shim will be removed in django CMS 4.3 release. For now,
they raise a ``RemovedInDjangoCMS43Warning`` warning.

Most prominent changes to custom code are:

* Pages have a ``site`` field again: ``page.node.site`` becomes ``page.site``
* ``page.node.path`` becomes ``page.path``
* ``page.node.depth`` becomes ``page.depth``
* ``page.node.numchild`` becomes ``page.numchild``
* ``page.node.parent`` and ``page.page_parent`` become ``page.parent``

Please also check your ``.filter()``, ``.order()``, ``.select_related()``, and
``.prefetch_related()`` calls to ensure they are still correct:
``.filter(node__site=site)`` becomes ``.filter(site=site)`` etc.

If you have custom code that accesses the ``Page.node`` attribute, you should
update it to use the new attributes on the ``Page`` model.

Miscellaneous
=============

* The function ``cms.cms_menus.get_visible_nodes`` has been deprecated. For
  performance reasons, the ``cms_menus`` builds the navigation node list based
  on page content objects. Use ``cms.cms_menus.get_visible_page_contents``
  instead.

* The ``cms.test_utils.testcases.CMSTestCase`` class's ``assertWarns`` has been
  removed since it was an alias of ``CMSTestCase.failUnlessWarns`` and shadows
  Python's ``assertWarns``. In your test cases, use
  Python's ``assertWarns`` instead, or use the ``failUnlessWarns`` method
  of ``CMSTestCase`` which retains the syntax of the original method.

* ``CMSPluginBase.get_require_parent()``, ``CMSPluginBase.get_child_class_overrides()``,
  ``CMSPluginBase.get_child_plugin_candidates()``, ``CMSPluginBase.get_child_classes()``,
  ``CMSPluginBase.get_parent_classes()`` by default do receive ``None`` for their
  ``page`` argument.

Features deprecated in 4.2.0
============================

* Use of the ``node`` property of the :class:`~cms.models.pagemodel.Page` model
  is deprecated. Use its attributes on the :class:`~cms.models.pagemodel.Page`
  model directly instead.

Removal of deprecated functionality
===================================

* Built-in alias plugin: The alias plugin has been removed. If you need
  this functionality, you can use the ``djangocms-alias`` package.

* ``SuperLazyIterator``: This class has been removed. If you need this
  functionality, you can use the ``django.utils.functional.lazy``.

* ``LazyChoiceField``: This class has been removed. If you need this
  functionality, you can use the default ``django.forms.fields.ChoiceField`` class.

* ``SlugWidget``: This class has been removed from ``cms.wizard.forms``. If you
  need this functionality, you can use the ``cms.admin.forms.SlugWidget`` class.
